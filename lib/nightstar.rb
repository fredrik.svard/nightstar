require 'nightstar/version'
require 'rest-client'
module Nightstar
  class Fund
  	BASE_URL = 'https://www.morningstar.se/Funds/Quicktake/Overview.aspx?'
    def initialize(perfid: '0P0000IWH7')
      @perfid = perfid
    end

    #TODO: better name
    def nav
      url = "#{BASE_URL}perfid=#{@perfid}&programid=0000000000"
      response = do_it(url: url)
      fund = response.scan(%r{<h2>(.*)</h2>})
      cropped_data = response.scan(%r{<td>Senaste NAV</td><td>([,\d\ ]+)([\w]+)</td><td>([\d\-]+)</td>})
      { :fund => fund.flatten.first, :nav => cropped_data.flatten.first.tr(',', '.').to_f, :currency => cropped_data.flatten[1], :the_date => cropped_data.flatten.last}
    end

    def do_it(url: nil)
      RestClient.get(url, :user_agent => 'tutan').body
    end
  end
end
