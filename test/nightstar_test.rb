require_relative './test_helper'

class FakerReq
  attr_accessor :req, :resp
  def do_it(url: nil); end
end

DIDNERS_SMA =  '0P0000IWH7'
AVANZA_ZERO =  '0P00005U1J'
SEB_EUROLAND = '0P0000WJF0'

class NightstarTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Nightstar::VERSION
  end

  def test_get_nav_mock
    didner = Nightstar::Fund.new

    def didner.do_it(url: nil)
      text = %(<h2>SEB Rysslandfond C EUR - Lux</h2>uicktake1_col333_OverviewGeneralItem1_ctl04\" class=\"FlowLayout 
        Default  layout content\">\r\n\t\t\t\t\t\t<table class=\"alternated
        toplist 
        halftoplist\" border=\"0\">\r\n\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t
        <td>Senaste NAV</td><td>  738,16 SEK</td><td>2018-11-16</td>\r\n\t\t\t\t\t\t\t</tr><tr)
      fake = FakerReq.new
      fake.resp = text
    end
    assert_instance_of Float, didner.nav[:nav]

    assert_equal 738.16, didner.nav[:nav]

    assert_instance_of String, didner.nav[:fund]
    assert_instance_of String, didner.nav[:currency]
    assert_instance_of DateTime, DateTime.parse(didner.nav[:the_date])
   end
  
  def test_get_nav_didner
    didner = Nightstar::Fund.new(perfid: DIDNERS_SMA)
    assert didner.nav
    p didner.nav[:fund]
  end

  def test_get_nav_other
    other = Nightstar::Fund.new(perfid: SEB_EUROLAND) # perfid:
    assert_instance_of Float, other.nav[:nav]
    p other.nav[:fund]
    end

  def test_get_nav_other_2
    other = Nightstar::Fund.new(perfid: AVANZA_ZERO) # perfid:
    assert_instance_of Float, other.nav[:nav]
    p other.nav[:fund]
  end

  def test_get_nav_invalid_perfid
    assert_raises (NoMethodError)  { Nightstar::Fund.new(perfid: '0P005U1J').nav } 
  end

  def test_get_nav_invalid_method
    assert_raises (NoMethodError)  { Nightstar::Fund.new(perfid: '0P005U1J').nave } 
  end
end
