lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'nightstar/version'

Gem::Specification.new do |spec|
  spec.name          = 'nightstar'
  spec.version       = Nightstar::VERSION
  spec.authors       = ['Fredrik Svard']
  spec.email         = ['fredrik.svard@gmail.com']

  spec.summary       = 'Read net asset value, nav from a specific site'
  spec.description   = 'Read net asset value, nav from a specific site, by web scraping a specific site'
  spec.homepage      = 'http://example.com'

  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'https://rubygems.org'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'rest-client'

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'minitest', '~> 5.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'standard'
end
